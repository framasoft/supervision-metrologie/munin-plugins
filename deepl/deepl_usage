#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

  deepl_usage - plugin to get stats from Mobilizon

=head1 AUTHOR AND COPYRIGHT

  Copyright 2020 Framasoft — Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/deepl_usage

     [deepl_usage]
     env.apikey aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee:fx
     env.url https://api-free.deepl.com/v2/

=item - env.apikey

  This is your Deepl API key.
  See L<https://www.deepl.com/fr/docs-api/accessing-the-api/api-versions/#authentication>
  when logged in.

=item - env.url

  This is the URL of the API 
  See L<https://www.deepl.com/fr/docs-api/accessing-the-api/api-versions/#authentication>
  when logged in.

=item - /etc/munin/plugins

     ln -s deepl_usage /etc/munin/plugins/deepl_usage

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojolicious.

  Install it trough the cpan command:

     cpan Mojolicious

  You will need IO::Socket::SSL (may require libssl-dev to install)

     cpan IO::Socket::SSL

  Or on Debian:

     apt install libmojolicious-perl libio-socket-ssl-perl

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::URL;
use Mojo::UserAgent;

my $instance= $Munin::Plugin::me;
my $PLUGIN_NAME = $instance;
$instance =~ s/deepl_usage//;

munin_exit_fail() unless (defined($ENV{apikey} && defined($ENV{url})));

my $apikey = $ENV{apikey};
my $url    = $ENV{url};

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print <<EOF;
graph_title Deepl API usage
graph_args --lower-limit 0
graph_category deepl
usage.label Usage (char. nb)
usage.draw AREA
usage.colour 0000FF
limit.label Limit (char. nb)
limit.draw LINE
limit.colour FF0000
EOF
    munin_exit_done();
}

##### fetch
my $u  = Mojo::URL->new($url);
$u->path($u->path->merge('usage'));
my $json = Mojo::UserAgent->new()
                          ->get($u => { Authorization => sprintf('DeepL-Auth-Key %s', $apikey) })
                          ->result
                          ->json;
print <<EOF;
usage.value $json->{character_count}
limit.value $json->{character_limit}
EOF
munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
