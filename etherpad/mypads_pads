#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

  mypads_pads - plugin to get stats from etherpad lite (must have the ep_mypads plugin)

=head1 AUTHOR AND COPYRIGHT

  Copyright 2020 Framasoft

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/mypads_pads

     [mypads_pads]
     env.url https://mypads.example.org/mypads/api/stats/stats.json

=item - env.url

  This is the URL of the etherpad instance to get stats from (without trailing slash)

  You can specify the port, or a login and a password for HTTP authentication

     https://login:password@mypads.example.org:9001

=item - /etc/munin/plugins

     ln -s mypads_pads /etc/munin/plugins/mypads_pads

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojolicious

  Although it is certainly available in your GNU/Linux packages, it recommended to install it trough the cpan command:

     cpan Mojolicious

  If you want to monitor secured etherpad instances (HTTPS), you need IO::Socket::SSL (may require libssl-dev to install)

     cpan IO::Socket::SSL

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::UserAgent;
use Mojo::JSON qw(decode_json);

my $PLUGIN_NAME = 'mypads_pads';

munin_exit_fail() unless (defined($ENV{url}));

my $url = $ENV{url};

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print "graph_title Mypads pads\n";
    print "graph_vlabel Number of pads\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category etherpad\n";
    print "graph_total Total\n";
    print "graph_info This graph shows the count of pads on a mypads instance\n";
    print "pads.label Number of pads\n";
    print "pads.draw AREA\n";
    munin_exit_done();
}

##### fetch
my $ua = Mojo::UserAgent->new();
my %cache;
my $u  = Mojo::URL->new($url);
my $tx = $ua->get($url);
my $res = $tx->res;
if ($res->is_success) {
    my $r = decode_json($res->body);
    print 'pads.value ', $r->{pad}, "\n";
}
munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
