#!/bin/sh
# -*- sh -*-

: <<=cut

=head1 NAME

postfix_maildelivered - Plugin to monitor postfix delivered mails number

=head1 HOWTO CONFIGURE AND USE :

First, install pflogsumm

    apt-get install pflogsumm

=over

=item - /etc/munin/plugin-conf.d/postfix_delivered

    [postfix_maildelivered]
    user root

    If you want to use a different syslog name to look for in mail.log:

    [postfix_maildelivered_*]
    user root

=item - /etc/munin/plugins

    ln -s postfix_maildelivered_ /etc/munin/plugins/postfix_maildelivered

    If you want to use a different syslog name to look for in mail.log:

    ln -s postfix_maildelivered_ /etc/munin/plugins/postfix_maildelivered_other-syslog-name

=item - restart Munin node

    service munin-node restart

=head1 AUTHOR

Luc Didry <luc@framasoft.org>

=head1 LICENSE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

. "$MUNIN_LIBDIR/plugins/plugin.sh"

plugin_name=$(basename "$0")

if [ "$plugin_name" = 'postfix_maildelivered' ]; then
    syslog_name='postfix'
    title="Postfix delivered mails by day"
else
    syslog_name=${plugin_name#postfix_maildelivered_}
    title="Postfix delivered mails by day ($syslog_name)"
fi

case $1 in
    config)
        cat <<EOF
graph_title $title
graph_vlabel Delivered mails
graph_category postfix
delivered.label delivered
EOF
        exit 0
        ;;
esac

value=$(sed "s@$syslog_name-[^/]\+/@$syslog_name/@" /var/log/mail.log | pflogsumm -d today --smtpd-stats --detail 0 --syslog-name="$syslog_name" 2>/dev/null |
            grep " \+[0-9]\+ \+delivered" |
            sed -e "s@ \+\([0-9]\+\) \+delivered@\1@")
echo "delivered.value $value"
