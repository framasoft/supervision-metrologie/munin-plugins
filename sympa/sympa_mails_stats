#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=head1 NAME

  sympa_mails_stats - Used to track how many lists are opened on your Sympa server

=head1 AUTHOR AND COPYRIGHT

  Copyright 2016 Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/sympa_mails_stats

     [sympa_mails_stats]
     user postgres
     env.exclude list1,list2

=item - /etc/munin/plugins

     ln -s sympa_mails_stats /etc/munin/plugins/sympa_mails_stats

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojo::Pg

  Although it is certainly available in your GNU/Linux packages, it recommended to install it trough the cpan command:

     cpan Mojo::Pg

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::Pg;
use Mojo::Collection 'c';

my $PLUGIN_NAME = 'sympa_mails_stats';
my $exclude = (defined($ENV{exclude})) ? c(split(',', $ENV{exclude})) : c();

my $pg = Mojo::Pg->new('postgresql://postgres@/sympa');

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    print "graph_title Sympa sent mails\n";
    print "graph_vlabel Number of mails sent in the last 24h on Sympa\n";
    print "graph_args --lower-limit 0\n";
    print "graph_category sympa\n";
    print "graph_info This graph shows some stats about the number of mails sent in the last 24h on your Sympa server\n";
    print "avg.label Avg nb of mails sent in the last 24h/list\n";
    print "avg.draw LINE\n";
    print "min.label Min nb of mails sent in the last 24h/list\n";
    print "min.draw LINE\n";
    print "max.label Max nb of mails sent in the last 24h/list\n";
    print "max.draw LINE\n";
    print "total.label Total\n";
    print "total.draw LINE\n";
    munin_exit_done();
}

##### fetch
my $result = $pg->db->query('SELECT SUM(count_counter), list_counter FROM stat_counter_table WHERE data_counter = \'send_mail\' AND (current_timestamp - to_timestamp(beginning_date_counter)) < INTERVAL \'1 DAY\' GROUP BY list_counter')->hashes;
my $lists  = $result->size;
my $mails  = 0;
my $max    = 0;
my $min;
$result->each(
    sub {
        my ($e, $num) = @_;
        unless ($exclude->grep(sub {$_ eq $e->{list_counter}})->size) {
            $max    = $e->{sum} if ($e->{sum} > $max);
            $min    = $e->{sum} if (!defined($min) || $e->{sum} < $min);
        }
        $mails += $e->{sum};
    }
);
$min = 0 unless (defined $min);
print "avg.value ".sprintf("%.2f", $mails / $lists)."\n" if $lists;
print "avg.value 0\n" unless $lists;

print "min.value $min\n";
print "max.value $max\n";
print "total.value $mails\n";

munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
