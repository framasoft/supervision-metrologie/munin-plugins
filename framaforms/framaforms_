#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=encoding UTF-8
=head1 NAME

  framaforms_ - plugin to get stats from Framaforms

=head1 AUTHOR AND COPYRIGHT

  Copyright 2020 Framasoft — Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/framaforms_

     [framaforms_*]
     env.url https://framaforms.org/framaforms.json

=item - env.url

  This is the URL of the Framaforms instance to get stats from (with full path to the JSON file)

=item - /etc/munin/plugins

     ln -s framaforms_ /etc/munin/plugins/framaforms_nbUsers
     ln -s framaforms_ /etc/munin/plugins/framaforms_nbForms
     ln -s framaforms_ /etc/munin/plugins/framaforms_nbSubmissions

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojolicious

  Install it trough the cpan command:

     cpan Mojolicious

  If you want to monitor secured etherpad instances (HTTPS), you need IO::Socket::SSL (may require libssl-dev to install)

     cpan IO::Socket::SSL

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::URL;
use Mojo::UserAgent;

my $metric= $Munin::Plugin::me;
my $PLUGIN_NAME = $metric;
$metric =~ s/framaforms_//;

munin_exit_fail() unless (defined($ENV{url}));

my $url = $ENV{url};

##### config
if( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    my $opened = $metric;
    $opened = 'registered users' if ($metric eq 'nbUsers');
    $opened = 'forms' if ($metric eq 'nbForms');
    $opened = 'submissions' if ($metric eq 'nbSubmissions');

    my $u    = Mojo::URL->new($url);
    my $host = $u->host;

    printf("graph_title %s on %s\n", ucfirst($opened), $u->host);
    printf("graph_vlabel Number of %s\n", $opened);
    print "graph_args --lower-limit 0\n";
    print "graph_category framaforms\n";
    printf("graph_info This graph shows the number of %s\n", $opened);
    $host    =~ s/\./_/g;
    print "$host.label $host\n";
    print "$host.draw AREA\n";
    munin_exit_done();
}

##### fetch
my $ua    = Mojo::UserAgent->new();
my $json  = $ua->get($url)->result->json;
my $u     = Mojo::URL->new($url);
my $host  = $u->host;
my $value = $json->{$metric};
if (ref $value eq 'HASH') {
    $value = $json->{$metric}->{total_count};
}
$host    =~ s/\./_/g;
print $host, '.value ', $value;
munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
