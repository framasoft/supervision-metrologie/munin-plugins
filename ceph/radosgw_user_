#!/usr/bin/perl
# vim: set filetype=perl sw=4 tabstop=4 expandtab smartindent: #

=encoding UTF-8
=head1 NAME

  radosgw_user_ - plugin to get stats about a Ceph radosgw user

=head1 AUTHOR AND COPYRIGHT

  Copyright 2020 Framasoft — Luc Didry <luc AT framasoft.org>

=head1 HOWTO CONFIGURE AND USE :

=over

=item - /etc/munin/plugin-conf.d/radosgw_user_

     [radosgw_user_*]
     user root

=item - /etc/munin/plugins

     ln -s radosgw_user_ /etc/munin/plugins/radosgw_user_foo
     ln -s radosgw_user_ /etc/munin/plugins/radosgw_user_bar

=item - restart Munin node

     service munin-node restart

=back

=head1 DEPENDENCIES

  You will need the Perl distribution Mojolicious

  Install it trough the cpan command:

     cpan Mojolicious

  Or on Debian:

    apt install libmojolicious-perl

=head1 LICENSE

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use Munin::Plugin;
use Mojo::JSON qw(decode_json);

my $user= $Munin::Plugin::me;
my $PLUGIN_NAME = $user;
$user =~ s/radosgw_user_//;

##### config
if ( (defined $ARGV[0]) && ($ARGV[0] eq 'config') ) {
    printf("multigraph %s\n", $PLUGIN_NAME);
    printf("graph_title Ceph radosgw stats for user %s\n", $user);
    print "graph_args --lower-limit 0\n";
    print "graph_category radosgw\n";
    printf("graph_info This graph shows the Ceph radosgw stats for user %s\n", $user);
    for my $measure (qw/entries bytes/) {
        my $label = ($measure eq 'entries') ? 'objects' : 'disk used (in bytes)';
        printf("%s.label %s\n", $measure, $label);
        printf("%s.draw LINE\n", $measure);
    }
    for my $measure (qw/entries bytes/) {
        my $label = ($measure eq 'entries') ? 'objects' : 'disk used (in bytes)';
        printf("multigraph %s.%s\n", $PLUGIN_NAME, $measure);
        printf("graph_title Ceph radosgw stats for user %s: %s\n", $user, ucfirst($label));
        print "graph_args --lower-limit 0\n";
        print "graph_category radosgw\n";
        printf("%s.label %s\n", $measure, $label);
        printf("%s.draw LINE\n", $measure);
    }
    munin_exit_done();
}

##### fetch
open my $ph, "radosgw-admin user stats --uid=$user |" or die "Unable to use radosgw-admin: $!";
my $json_text = '';
while (<$ph>) {
    $json_text .= $_;
}
my $json  = decode_json($json_text);

for my $measure (qw/entries bytes/) {
    my $key = 'size_'.$measure;
    $key = 'num_objects' if $measure eq 'entries';
    $key = 'size_actual' if $measure eq 'bytes';
    printf("%s.value %s\n", $measure, $json->{stats}->{$key});
}
for my $measure (qw/entries bytes/) {
    my $key = 'size_'.$measure;
    $key = 'num_objects' if $measure eq 'entries';
    $key = 'size_actual' if $measure eq 'bytes';
    printf("multigraph %s.%s\n", $PLUGIN_NAME, $measure);
    printf("%s.value %s\n", $measure, $json->{stats}->{$key});
}
munin_exit_done();

#
##
### INTERNALS FONCTIONS
###############################################################################
sub munin_exit_done {
    munin_exit(0);
} ## sub munin_exit_done

sub munin_exit_fail {
    munin_exit(1);
} ## sub munin_exit_fail

sub munin_exit {
    my $exitcode = shift;
    exit($exitcode) if(defined $exitcode);
    exit(1);
} ## sub munin_exit
